[![Latest Release](https://gitlab.com/fton/const-frac/-/badges/release.svg)](https://gitlab.com/fton/const-frac/-/releases)
[![pipeline status](https://gitlab.com/fton/const-frac/badges/main/pipeline.svg)](https://gitlab.com/fton/const-frac/-/commits/main)
[![coverage report](https://gitlab.com/fton/const-frac/badges/main/coverage.svg)](https://gitlab.com/fton/const-frac/-/commits/main)
# Description
Utilities for calculating floating point constants in const context.

This package is not intended heavy numerical calculation but just evaluating floating point constants in const contest.
# Usage
```rust
use ::const_frac::Frac;

const RAD2DEG: f64 = Frac::from_ratio(360, 2, 0).mul(Frac::from_exp_pi(-1)).to_f64();

assert_eq!(RAD2DEG * std::f64::consts::PI, 180f64);
```
# Features
This package has the following feature flags.
No feature flags are enabled by *default*.

- **`std`**
  Enable std support.
- **`tokenize`**
  Enable [Parse] and [ToToken] implementation for Frac when using in implementation of a procedural macro.
- **`ratio`**
  Enable [num_rational::BigRational] supports.

# Project status
This package is in the very early stage.

[Parse]: https://docs.rs/syn/*/syn/parse/trait.Parse.html
[ToToken]: https://docs.rs/quote/*/quote/trait.ToTokens.html
[num_rational::BigRational]: https://docs.rs/num-rational/*/num_rational/type.BigRational.html