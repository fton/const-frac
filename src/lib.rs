#![doc = include_str!("../README.md")]
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(feature = "tokenize")]
#[cfg_attr(docsrs, doc(cfg(feature = "tokenize")))]
pub mod frac;

#[cfg(not(feature = "tokenize"))]
mod frac;

mod real;
pub mod error;

pub use frac::Frac;
pub use real::Real;
