use core::fmt:: { self, Debug, Display };
use combine::error::StringStreamError;

#[cfg(feature = "std")]
use std;

/// Error for this package.
/// Maybe change.
#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum Error {
    NotF64,
    NotI32,
    ZeroDivide,
    Overflow,
    ParseError(StringStreamError),
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        Debug::fmt(self, f)
    }
}

#[cfg(feature = "std")]
#[cfg_attr(docsrs, doc(cfg(feature = "std")))]
impl std::error::Error for Error {}